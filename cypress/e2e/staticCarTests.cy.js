/// <reference types="cypress" />
import * as car from "../support/page_objects/mainPage"

describe('Tests for static car: buttons work as expected', () => {

  beforeEach(() => {
    car.visit()
  })

  afterEach(() => {
    car.stopExperiment()
  })

  it('Front label is visible', () => {
    cy.get(car.label.class).should('have.text', car.label.txt)
  })

  it('The left door works and looks like as expected', () => {
    cy.get(car.leftDoor.path).should('have.class', car.leftDoor.mainClass)

    car.openLeftDoor()
    cy.get(car.leftDoorButton.class).should('have.text', car.leftDoorButton.closeTxt)
    cy.get(car.leftDoor.path).should('have.class', car.leftDoor.isOpenClass)
    
    car.closeLeftDoor()
    cy.get(car.leftDoorButton.class).should('have.text', car.leftDoorButton.openTxt)
    cy.get(car.leftDoor.path).should('not.have.class', car.leftDoor.isOpenClass)
  })

  it('The right door works as expected', () => {
  cy.get(car.rightDoor.path).should('have.class', car.rightDoor.mainClass)
    car.openRightDoor()
    cy.get(car.rightDoorButton.class).should('have.text', car.rightDoorButton.closeTxt)
    cy.get(car.rightDoor.path).should('have.class', car.rightDoor.isOpenClass)

    car.closeRightDoor()
    cy.get(car.rightDoorButton.class).should('have.text', car.rightDoorButton.openTxt)
    cy.get(car.rightDoor.path).should('not.have.class', car.rightDoor.isOpenClass)
  })

  it('The honk works as expected', () => {
    car.pressHonk()
    cy.get(car.honkButton.class).should('have.text', car.honkButton.txt)
  })

  it('The start button works as expected', () => {
    car.startExperiment()
    cy.get(car.road.class).should('have.class', car.road.movingClass)
  })

  it('The restart button works as expected', () => {
    car.startExperiment()
    cy.get(car.road.class).should('have.class', car.road.movingClass)

    car.restartExperiment()
    cy.get(car.road.class).should('not.have.class', car.road.movingClass)
  })

})
