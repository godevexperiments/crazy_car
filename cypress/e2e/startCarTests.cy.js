/// <reference types="cypress" />
import * as car from "../support/page_objects/mainPage"

describe('Tests start the moving', () => {

  beforeEach(() => {
    car.visit()
  })

  afterEach(() => {
    car.stopExperiment()
  })

  it('Car cannot start if left door is opened', () => {
    car.openLeftDoor()
    cy.get(car.leftDoor.path).should('have.class', car.leftDoor.isOpenClass)

    car.startExperiment()
    cy.get(car.stopSign.path).should('have.attr', 'style', car.stopSign.style)
    cy.get(car.stopSign.stoppedSignPath).should('have.text', car.stopSign.stoppedTxt)
    cy.get(car.stopSign.alertPath).should('have.text', car.stopSign.alertTxt)
    
    cy.get(car.leftDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.rightDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.honkButton.class).should('have.attr', 'disabled')
    cy.get(car.restartButton.class).should('have.attr', 'style', 'display: inline-block;')

  })

  it('Car cannot start if right door is opened', () => {
    car.openRightDoor()
    cy.get(car.rightDoor.path).should('have.class', car.rightDoor.isOpenClass)

    car.startExperiment()
    cy.get(car.stopSign.path).should('have.attr', 'style', car.stopSign.style)
    cy.get(car.stopSign.stoppedSignPath).should('have.text', car.stopSign.stoppedTxt)
    cy.get(car.stopSign.alertPath).should('have.text', car.stopSign.alertTxt)
    
    cy.get(car.leftDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.rightDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.honkButton.class).should('have.attr', 'disabled')
    cy.get(car.restartButton.class).should('have.attr', 'style', 'display: inline-block;')

  })

  it('Car cannot start if both doors are opened', () => {
    car.openRightDoor()
    cy.get(car.rightDoor.path).should('have.class', car.rightDoor.isOpenClass)
    car.openLeftDoor()
    cy.get(car.leftDoor.path).should('have.class', car.leftDoor.isOpenClass)

    car.startExperiment()
    cy.get(car.stopSign.path).should('have.attr', 'style', car.stopSign.style)
    cy.get(car.stopSign.stoppedSignPath).should('have.text', car.stopSign.stoppedTxt)
    cy.get(car.stopSign.alertPath).should('have.text', car.stopSign.alertTxt)
    
    cy.get(car.leftDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.rightDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.honkButton.class).should('have.attr', 'disabled')
    cy.get(car.restartButton.class).should('have.attr', 'style', 'display: inline-block;')

  })

  it('Car can start if I honk', () => {
    car.pressHonk()
    cy.get(car.honkButton.class).should('have.text', car.honkButton.txt)
    car.startExperiment()
    cy.get(car.road.class).should('have.class', car.road.movingClass)
  })

  it('Car can start if I close left door', () => {
    car.openLeftDoor()
    cy.get(car.leftDoor.path).should('have.class', car.leftDoor.isOpenClass)
    car.closeLeftDoor()
    cy.get(car.leftDoor.path).should('not.have.class', car.leftDoor.isOpenClass)

    car.startExperiment()
    cy.get(car.road.class).should('have.class', car.road.movingClass)
  })

    it('Car can start if I close right door', () => {
        car.openRightDoor()
        cy.get(car.rightDoor.path).should('have.class', car.rightDoor.isOpenClass)
        car.closeRightDoor()
        cy.get(car.rightDoor.path).should('not.have.class', car.rightDoor.isOpenClass)
    
        car.startExperiment()
        cy.get(car.road.class).should('have.class', car.road.movingClass)
    })

    it('Car can start if I close both doors', () => {
        car.openRightDoor()
        cy.get(car.rightDoor.path).should('have.class', car.rightDoor.isOpenClass)
        car.openLeftDoor()
        cy.get(car.leftDoor.path).should('have.class', car.leftDoor.isOpenClass)
        car.closeRightDoor()
        cy.get(car.rightDoor.path).should('not.have.class', car.rightDoor.isOpenClass)
        car.closeLeftDoor()
        cy.get(car.leftDoor.path).should('not.have.class', car.leftDoor.isOpenClass)
    
        car.startExperiment()
        cy.get(car.road.class).should('have.class', car.road.movingClass)
    })

})
