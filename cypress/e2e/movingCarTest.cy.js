/// <reference types="cypress" />
import * as car from "../support/page_objects/mainPage"

describe('Tests start the moving', () => {

  beforeEach(() => {
    car.visit()
    car.startExperiment()
    cy.get(car.road.class).should('have.class', car.road.movingClass)
  })

  afterEach(() => {
    car.stopExperiment()
  })

  it('Car stops if left door is opened', () => {
    car.openLeftDoor()
    cy.get(car.leftDoor.path).should('have.class', car.leftDoor.isOpenClass)

    cy.get(car.stopSign.path).should('have.attr', 'style', car.stopSign.style)   
    cy.get(car.stopSign.stoppedPath).should('have.text', car.stopSign.stoppedTxt)
    cy.get(car.stopSign.alertPath).should('have.text', car.stopSign.alertTxt)
    
    cy.get(car.leftDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.rightDoorButton.class).should('have.attr', 'disabled')
    cy.get(car.honkButton.class).should('have.attr', 'disabled')
    cy.get(car.restartButton.class).should('have.attr', 'style', 'display: inline-block;')

  })

  it('Car stops if right door is opened', () => {
      car.openRightDoor()
      cy.get(car.rightDoor.path).should('have.class', car.rightDoor.isOpenClass)
  
      cy.get(car.stopSign.path).should('have.attr', 'style', car.stopSign.style)
      cy.get(car.stopSign.stoppedSignPath).should('have.text', car.stopSign.stoppedTxt)
      cy.get(car.stopSign.alertPath).should('have.text', car.stopSign.alertTxt)
      
      cy.get(car.leftDoorButton.class).should('have.attr', 'disabled')
      cy.get(car.rightDoorButton.class).should('have.attr', 'disabled')
      cy.get(car.honkButton.class).should('have.attr', 'disabled')
      cy.get(car.restartButton.class).should('have.attr', 'style', 'display: inline-block;')
  
  })

    /* There is one bug - the car should stop if both doors are opened, but it doesn't if the left door is opened first. 
    and the right door is opened after that with delay 5sec.
    But there is no reason for specific test for that, because we have one which checked left door. And if it passed,
    we can't get the case with 2 doors opened.
    */

  it('I honk and continue driving as crazy', () => {
      car.pressHonk()
      cy.get(car.road.class).should('have.class', car.road.movingClass)
      
  });

})
