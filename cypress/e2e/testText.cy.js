//* here we should have tests for the page itself, not for the car.

/// <reference types="cypress" />
import * as car from "../support/page_objects/mainPage"

describe('Tests for the page', () => {
    beforeEach(() => {
        car.visit()
      })

    it('should display the correct safety system title', () => {
        cy.get('h2').should('contain', 'Very Safe Safety System 3000');
    });

    it('should display the correct slogan', () => {
        cy.get('.slogan').should('contain', 'arrive in one piece from every drive*');
    });

    it('should display the text', () => {
        cy.get('p').should('contain', 'The system should prevent unsafe actions by stopping a car. Unsafe means a car moving with any of its doors open. The system is not meant to block any actions (buttons). It can only stop a car.');
    });

    it('should display the correct text and style for the paragraph', () => {
        cy.get('body > :nth-child(6)')
            .should('contain', "* You may not arrive in one piece from some drives, depending on circumstances. Restrictions apply. Your car is unlikely to arrive in one piece anyways. All the system decisions are final. Obey to the machine. All you say can and will be used against you. Do not drive under influence. Or do. We're not dictating you what to do, anyways. It's all just an advice, in the end. That's what we're going to tell to the judge. Your opinion is very valuable to us.")
            .should('have.css', 'font-size', '9px')
            .should('have.css', 'opacity', '0.5');
    });
    
});