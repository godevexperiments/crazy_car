
// usually 'class' is very generic, but in this case it's ok
function doorButtonClick(classId, buttonTxt, positiveLogMessage, negativeLogMessage) {
    cy.get(classId).contains(buttonTxt).then(($button) => {
        if ($button.length > 0) {
            cy.log(positiveLogMessage);
            cy.wrap($button).click();
        } else {
            cy.log(negativeLogMessage);
        }
    });
}

export const label = {
    class: '.front-label',
    txt: 'front'
};

export const road = {
    class: '.road',
    movingClass: 'road-moving',
};

export const stopSign = {
    path: '.sign-saved',
    style: "visibility: visible;",
    stoppedSignPath: '.sign-saved > span',
    stoppedTxt: 'STOPPED',
    alertPath: '.sign-saved > div',
    alertTxt: 'system has prevented you from doing the wrong thing',
};

export const leftDoorButton = {
    class: '.js-left-door-button',
    openTxt: 'Open left door',
    closeTxt: 'Close left door',
    positiveLogMessage: 'Clicking the button to open the left door',
    negativeLogMessage: 'The left door is already open'
};

export const leftDoor = {
    path: 'div.door:nth-child(2)',
    mainClass: 'door door-left',
    isOpenClass: 'door--open',
};

export const rightDoorButton = {
    class: '.js-right-door-button',
    openTxt: 'Open right door',
    closeTxt: 'Close right door',
    positiveLogMessage: 'Clicking the button to open the right door',
    negativeLogMessage: 'The right door is already open'
};

export const rightDoor = {
    path: 'div.door:nth-child(1)',
    mainClass: 'door door-right',
    isOpenClass: 'door--open',
};

export const honkButton = {
    class: '.honk-button',
    txt: 'Honk',
    positiveLogMessage: 'The car is honking'
}

export const restartButton = {
    class: '.experiment-restart-button',
    txt: 'Restart experiment',
    negativeLogMessage: 'The car is already stopped and ready for the next test',
    positiveLogMessage: 'The car stops and ready for the next test',
    visibleStyle: 'display: inline-block'
}

export const startButton = {
    class: '.experiment-start-button',
    txt: 'Start driving',
    positiveLogMessage: 'The car is starting',
    negativeLogMessage: 'The car is already moving',
    invisibleStyle: 'display: none'
}

export function visit() {
    cy.visit('/');
}

export function openLeftDoor() {
    doorButtonClick(leftDoorButton.class, leftDoorButton.openTxt, leftDoorButton.positiveLogMessage, leftDoorButton.negativeLogMessage);
}

export function openRightDoor() {
    doorButtonClick(rightDoorButton.class, rightDoorButton.openTxt, rightDoorButton.positiveLogMessage, rightDoorButton.negativeLogMessage);
}

export function closeLeftDoor() {
    doorButtonClick(leftDoorButton.class, leftDoorButton.closeTxt, leftDoorButton.positiveLogMessage, leftDoorButton.negativeLogMessage);
}

export function closeRightDoor() {
    doorButtonClick(rightDoorButton.class, rightDoorButton.closeTxt, rightDoorButton.positiveLogMessage, rightDoorButton.negativeLogMessage);
}

export function pressHonk() {
    cy.get(honkButton.class).then(($button) => {
        cy.wrap($button).click();
        cy.log(honkButton.positiveLogMessage);
    });
}

export function stopExperiment() {
    cy.get(restartButton.class).then(($button) => {
        let style = $button.attr('style');
        if (style.includes(restartButton.visibleStyle)) {
            cy.wrap($button).click()
            cy.log(restartButton.positiveLogMessage)
        } else {
            cy.log(restartButton.negativeLogMessage)
        }
    })
}

export function startExperiment() {
    cy.get(startButton.class).then(($button) => {
        cy.wrap($button).click();
        cy.log(startButton.positiveLogMessage);
    });
}

export function restartExperiment() {
    cy.get(restartButton.class).then(($button) => {
        cy.wrap($button).click();
        cy.log(restartButton.positiveLogMessage);
    });
}
